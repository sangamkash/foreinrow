﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace FourInRow.Token
{
    public class TokenSpawner : MonoBehaviour
    {
        [SerializeField] private RectTransform m_RectTransform;
        [SerializeField] private GameObject m_Prefab;
        [SerializeField] private float m_OffsetHeight;
        [SerializeField] private float m_TimeFactor;
        [SerializeField] private float m_BounceFactor;
        [SerializeField] private Color m_PlayerColor;
        [SerializeField] private Color m_OpponentColor;
        private float mChildWidth;
        private int mPadding;
        private int mBoardSize;
        public void Init(int boardSize,int padding)
        {
            mPadding = padding;
            mBoardSize = boardSize;
            m_RectTransform = GetComponent<RectTransform>();
            var w = m_RectTransform.rect.width;
            mChildWidth = (w - 2 * padding) / boardSize;
        }
        
        public void SpawnToker(int x,int y,bool isAI,Action OnMovementStop)
        {
            var h = m_RectTransform.rect.height;
            var obj=Instantiate(m_Prefab, m_RectTransform);
            var token = obj.GetComponent<Token>();
            var xStartPos = (x + 0.5f) * mChildWidth+ mPadding;
            var yStartPos = h + m_OffsetHeight+mPadding;
            var xEndPos = xStartPos;
            var yEndPos = (y + 0.5f) * mChildWidth +mPadding;
            var color = isAI ? m_OpponentColor : m_PlayerColor;
            token.Init(mChildWidth,color,new Vector3(xStartPos,yStartPos,0f),new Vector3(xEndPos,yEndPos,0f),m_TimeFactor,(mBoardSize-y)*m_BounceFactor, OnMovementStop);
        }
    }
}
