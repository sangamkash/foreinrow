﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace FourInRow.Token
{
    public class Token : MonoBehaviour
    {
        [SerializeField] private RectTransform m_ThisRectTrans;
        [SerializeField] private Image m_Image;
        [SerializeField] private AnimationCurve m_BounceAnimationCurve;
        private float mAmplitude;
        public void Init(float size,Color color,Vector3 startposition,Vector3 endposition,float time,float amplitude,Action OnComplete)
        {
            mAmplitude = amplitude;
            m_Image.color = color;
            m_ThisRectTrans.sizeDelta = Vector2.one * size;
            StartCoroutine(DoMovement(time, startposition, endposition, OnComplete));
        }

        IEnumerator DoMovement(float time,Vector3 start,Vector3 end,Action OnComplete)
        {
            var timePeriod = time ;
            var tempTime = timePeriod;
            while (tempTime>0f)
            {
                var lerpValue = Mathf.Clamp01(1f - (tempTime / timePeriod));
                m_ThisRectTrans.anchoredPosition = Vector3.Lerp(start, end, lerpValue);
                tempTime -= Time.deltaTime+Time.deltaTime*9.8f*lerpValue;
                yield return new WaitForEndOfFrame();
            } 
            //BouncingEffect;
            timePeriod = time;
            tempTime = timePeriod;
            while (tempTime>0f)
            {
                var lerpValue = Mathf.Clamp01(1f - (tempTime / timePeriod));
                var lerp = Mathf.PingPong(lerpValue*5f,1f);
                m_ThisRectTrans.anchoredPosition = Vector3.Lerp(end, end + Vector3.up * mAmplitude *m_BounceAnimationCurve.Evaluate(1f- lerpValue), lerp);
                tempTime -= Time.deltaTime+Time.deltaTime*9.8f*lerpValue;
                yield return new WaitForEndOfFrame();
            }

            m_ThisRectTrans.anchoredPosition = end;
            OnComplete?.Invoke();
            
        }
    }
}