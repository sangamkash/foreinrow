﻿using UnityEngine;

namespace FourInRow.Matrix
{
    public static class MatrixExtension 
    {
        public static void CreateWithDefaultMatrix(this int [,] matrix,int size,int defaultValue=0)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = defaultValue;
                }
            }
        }

        public static int[,] ReturnMatrixCopy(this int[,] matrix, int size)
        {
            var newmatrix = new int [size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    newmatrix[i, j] = matrix[i,j];
                }
            }

            return newmatrix;
        }
        
        public static bool  CheckForWinCase(this int [,] matrix,int size,int minToWin)
        {
            for (int i = 0; i < size; i++)
            {
                if (Horizontalcheck(i,matrix,size,minToWin) > -1)
                    return true;
                if (Verticalcheck(i,matrix,size,minToWin) > -1)
                    return true;
            }

            if (DiagonalCheck1(matrix,size,minToWin) > -1)
                return true;
            if (DiagonalCheck2(matrix,size,minToWin) > -1)
                return true;
            return false;
        }
        public static bool ValidateInput(this int [,] matrix,int size,int row, out int coloumn)
        {
            var sum = 0;
            coloumn = -1;
            for (int j = 0; j < size; j++)
            {
                var temp = matrix[row, j];
                if (coloumn == -1 && temp == 0)
                {
                    var t = j;
                    coloumn = t;
                }
                sum += Mathf.Abs(temp);
            }

            return sum < size;
        }
        
        public static void UpdateValueInRow(this int [,] matrix,int size,int row, bool isAI)
        {
            var value = isAI ? -1 : 1;
            for (int j = 0; j < size; j++)
            {
                var temp = matrix[row, j];
                if (temp == 0)
                {
                    matrix[row, j] = value;
                    return;
                }
            }
        }
        
        public static void UndoValueInRow(this int [,] matrix,int size,int row)
        {
            for (int j = size-1; j >=0 ; j--)
            {
                var temp = matrix[row, j];
                if (temp != 0)
                {
                    matrix[row, j] = 0;
                    return;
                }
            }
        }
        
        private static int Horizontalcheck(int row,int [,] matrix,int size,int minToWin)
        {
            var player1 = 0;
            var player2 = 0;
            for (int i = 0; i < size; i++)
            {
                if (matrix[row, i] == 1)
                {
                    player1++;
                    player2 = 0;
                }
                else if (matrix[row, i] == -1)
                {
                    player1 = 0;
                    player2++;
                }
                else
                {
                    player1 = 0;
                    player2 = 0;
                }

                if (player1 >= minToWin)
                {
                    return 0;
                }
                if (player2 >= minToWin)
                {
                    return 1;
                }
            }

            return -1;
        }
        
        private static int Verticalcheck(int column,int [,] matrix,int size,int minToWin)
        {
            var player1 = 0;
            var player2 = 0;
            for (int i = 0; i < size; i++)
            {
                if (matrix[i, column] == 1)
                {
                    player1++;
                    player2 = 0;
                }
                else if (matrix[i, column] == -1)
                {
                    player1 = 0;
                    player2++;
                }
                else
                {
                    player1 = 0;
                    player2 = 0;
                }

                if (player1 >= minToWin)
                {
                    return 0;
                }
                if (player2 >= minToWin)
                {
                    return 1;
                }
            }
            return -1;
        }
        
        private static int DiagonalCheck1(int [,] matrix,int size,int minToWin)
        {
            //Row
            for (int i = 0; i <= minToWin; i++)
            {
                var player1 = 0;
                var player2 = 0;
                for (int j = 0; j <= i; j++)
                {
                    var value = matrix[i-j, j];
#if DEBUG_LOG
                    Debug.Log($"tag1=Index<color=yellow> [{i-j}{j}] </color>");
#endif
                    var returnValue= CheckValue(value,minToWin, ref player1, ref player2);
                    if (returnValue > -1)
                        return returnValue;
                }
#if DEBUG_LOG
                Debug.Log($"tag1=Index<color=yellow> ==============</color>");
#endif
            }
            for (int i = 1; i <= minToWin; i++)
            {
                var player1 = 0;
                var player2 = 0;
                var k = 0;
                for (int j = size-1; j >= i; j--)
                {
                    var value = matrix[j, i+k];
#if DEBUG_LOG
                    Debug.Log($"tag2=Index<color=yellow> [{j}{i+k}]  =={i}</color>");
#endif
                    k++;
                    var returnValue= CheckValue(value,minToWin, ref player1, ref player2);
                    if (returnValue > -1)
                        return returnValue;
                   
                }
#if DEBUG_LOG
                Debug.Log($"tag2=Index<color=yellow> ==============</color>");
#endif
            }
            return -1;
        }

        private static int DiagonalCheck2(int [,] matrix,int size,int minToWin)
        {
            //Row
            for (int i = 0; i < minToWin; i++)
            {
                var player1 = 0;
                var player2 = 0;
                for (int j = 0; j < size-i; j++)
                {
                    var value = matrix[j+i, j];
#if DEBUG_LOG
                    Debug.Log($"tag3=Index<color=yellow> [{j+i}{j}] </color>");
#endif
                    var returnValue= CheckValue(value,minToWin, ref player1, ref player2);
                    if (returnValue > -1)
                        return returnValue;
                }
#if DEBUG_LOG
                Debug.Log($"tag3=Index<color=yellow> ==============</color>");
#endif
            }
            for (int i = 0; i < minToWin; i++)
            {
                var player1 = 0;
                var player2 = 0;
                for (int j = 0; j < size-i; j++)
                {
                    var value = matrix[j, j+i];
#if DEBUG_LOG
                    Debug.Log($"tag4=Index<color=yellow> [{j}{j+i}] </color>");
#endif
                    var returnValue= CheckValue(value,minToWin, ref player1, ref player2);
                    if (returnValue > -1)
                        return returnValue;
                }
#if DEBUG_LOG
                Debug.Log($"tag4=Index<color=yellow> ==============</color>");
#endif
            }
            return -1;
        }
        private static int CheckValue(int value,int minToWin, ref int player1, ref int player2)
        {
            if (value == 1)
            {
                player1++;
                player2 = 0;
            }
            else if (value == -1)
            {
                player1 = 0;
                player2++;
            }
            else
            {
                player1 = 0;
                player2 = 0;
            }

            if (player1 >= minToWin)
            {
                return 0; 
            }

            if (player2 >= minToWin)
            {
                return 1; 
            }

            return -1;
        }
    }
}
