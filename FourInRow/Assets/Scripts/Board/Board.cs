﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FourInRow
{

    public class Board : MonoBehaviour
    {
        [SerializeField] private GameObject m_Prefab;
        [SerializeField] private GridLayoutGroup m_GridLayoutGroup;
        [SerializeField] private RectTransform m_container;

        public void DisplayGrid(int boardSize,int padding)
        {
            m_GridLayoutGroup.spacing=Vector2.zero;
            m_GridLayoutGroup.padding=new RectOffset(padding,padding,padding,padding);
            m_GridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
            m_GridLayoutGroup.constraintCount = boardSize;
            var w = m_container.rect.width;
            var childWith = (w-2*padding) / m_GridLayoutGroup.constraintCount;
            m_GridLayoutGroup.cellSize = Vector2.one * childWith;
            m_GridLayoutGroup.startCorner = GridLayoutGroup.Corner.UpperLeft;
            m_GridLayoutGroup.startAxis = GridLayoutGroup.Axis.Horizontal;
            m_GridLayoutGroup.childAlignment = TextAnchor.UpperLeft;
            var count = boardSize * boardSize;
            for (int i = 0; i < count; i++)
            {
                Instantiate(m_Prefab, m_container);
            }
        }

        #if UNITY_EDITOR
        public void Reset()
        {
            var count = m_container.childCount;
            for (int i = 0; i < count; i++)
            {
                DestroyImmediate(m_container.GetChild(0).gameObject);
            }
        }
        #endif
    }
}