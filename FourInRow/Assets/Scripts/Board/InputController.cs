﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace FourInRow
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private GameObject m_Prefab;
        [SerializeField] private HorizontalLayoutGroup m_HorizontalLayoutGroup;
        [SerializeField] private RectTransform m_container;
        private Action<int> mInputCallBack;
        public void Init(int boardSize,int padding)
        {
           
            m_HorizontalLayoutGroup.spacing=0;
            m_HorizontalLayoutGroup.padding=new RectOffset(padding,padding,padding,padding);
            var w = m_container.rect.width;
            for (int i = 0; i < boardSize; i++)
            {
                var obj=Instantiate(m_Prefab, m_container);
                var btn = obj.GetComponent<Button>();
                var callBackValue = i ;
                btn.onClick.AddListener(delegate { InputCallBack(callBackValue);  });
            }
            gameObject.SetActive(false);
        }

        public void SubscribeToInput(Action<int> inputCallBack)
        {
            mInputCallBack = inputCallBack;
        }
        public void UnSubscribeToInput()
        {
            mInputCallBack = null;
        }

        private void InputCallBack(int value)
        {
            mInputCallBack?.Invoke(value);
        }
    }
}