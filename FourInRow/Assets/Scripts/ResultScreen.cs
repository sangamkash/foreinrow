﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FourInRow
{
    public class ResultScreen : MonoBehaviour
    {
        [SerializeField] private Text m_resultTxt;

        public void ShowResult(bool draw,string playerName)
        {
            if (draw)
                m_resultTxt.text = "Tie...!!";
            else
            {
                m_resultTxt.text = $"{playerName} won the match";
            }
         
            gameObject.SetActive(true);
        }

        public void BackBtn()
        {
            SceneManager.LoadScene(GameConstants.SCENE_STARTUP);
        }
        public void RetryBtn()
        {
            SceneManager.LoadScene(GameConstants.SCENE_GAMEPLAY);
        }
    }
}
