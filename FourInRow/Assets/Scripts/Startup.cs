﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FourInRow
{
    public class Startup : MonoBehaviour
    {
        [SerializeField] private Text m_PlayerNameTxt;
        [SerializeField] private Slider m_GridSizeSlider;
        [SerializeField] private Text m_GridSizeTxt;
        [SerializeField] private Slider m_MinToWinSlider;
        [SerializeField] private Text m_MinToWinTxt;
        [SerializeField] private GameObject m_PopupWarning;
        [SerializeField] private Dropdown m_LevelDropDown;
        private string mPlayerName;
        private int mGridSize, mMinToWin,mLevel;

        private void Awake()
        {
            mGridSize = 4;
            mMinToWin = 3;
            mLevel = 0;
        }

        public void Start()
        {
            var options = new List<string>(){"Easy", "Normal", "Hard"};
            m_LevelDropDown.ClearOptions();
            m_LevelDropDown.AddOptions(options);
            if (PlayerPrefs.HasKey(GameConstants.PP_PLAYER_NAME))
            {
                mPlayerName = PlayerPrefs.GetString(GameConstants.PP_PLAYER_NAME);
                m_PlayerNameTxt.text = mPlayerName;
            }
            if (PlayerPrefs.HasKey(GameConstants.PP_GRID_SIZE))
            {
                mGridSize = PlayerPrefs.GetInt(GameConstants.PP_GRID_SIZE);
                m_GridSizeTxt.text = mGridSize.ToString();
            }
            if (PlayerPrefs.HasKey(GameConstants.PP_MIN_TO_WIN))
            {
                mMinToWin = PlayerPrefs.GetInt(GameConstants.PP_MIN_TO_WIN);
                m_MinToWinTxt.text = mMinToWin.ToString();
            }
            if (PlayerPrefs.HasKey(GameConstants.PP_DIFFICULTY_LEVEL))
            {
                mLevel = PlayerPrefs.GetInt(GameConstants.PP_DIFFICULTY_LEVEL);
                m_LevelDropDown.value = mLevel;
            }
        }

        public void OnGridSizeUpdate()
        {
            var value = (int)m_GridSizeSlider.value;
            mGridSize = value;
            m_GridSizeTxt.text = mGridSize.ToString();
        }
        
        public void OnMintoWinUpdate()
        {
            var value = (int)m_MinToWinSlider.value;
            mMinToWin = value;
            m_MinToWinTxt.text = mMinToWin.ToString();
        }

        public void OnPlayerNameChange(string value)
        {
            m_PlayerNameTxt.text = value;
            mPlayerName = value;
        }

        public void CloseWarningPopup()
        {
            m_PopupWarning.SetActive(false);
        }

        public void LoadGame()
        {
            if (mGridSize > mMinToWin)
            {
                SaveData();
                SceneManager.LoadScene(GameConstants.SCENE_GAMEPLAY);
            }
            else
            {
                m_PopupWarning.SetActive(true);
            }
        }

        private void SaveData()
        {
            mLevel = m_LevelDropDown.value;
            PlayerPrefs.SetString(GameConstants.PP_PLAYER_NAME, mPlayerName);
            PlayerPrefs.SetInt(GameConstants.PP_GRID_SIZE, mGridSize);
            PlayerPrefs.SetInt(GameConstants.PP_MIN_TO_WIN, mMinToWin);
            PlayerPrefs.SetInt(GameConstants.PP_DIFFICULTY_LEVEL, mLevel);
        }
    }
}
