﻿using System;
using FourInRow.AIStrategy;
using UnityEngine;

namespace FourInRow.Player
{
    public abstract class Player:MonoBehaviour
    {
        protected Action<int, bool> mPerformAction;
        protected IStrategy mStrategy;
        public string _PlayerName;
        public void Init(Action<int, bool> performAction,IStrategy strategy,string playerName)
        {
            mPerformAction = performAction;
            mStrategy = strategy;
            _PlayerName = playerName;
        }
        public abstract void PlayTurn();
    }
}
