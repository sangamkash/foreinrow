﻿using UnityEngine;

namespace FourInRow.Player
{
    public class AIPlayer : Player
    {
        [SerializeField] private InputController m_InputController;
        public override void PlayTurn()
        {
            mPerformAction?.Invoke(mStrategy.ReturnBestChoice(),true);
        }
    }
}
