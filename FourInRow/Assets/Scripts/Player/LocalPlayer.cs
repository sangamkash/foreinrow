﻿using UnityEngine;

namespace FourInRow.Player
{
    public class LocalPlayer : Player
    {
        [SerializeField] private InputController m_InputController;
        public override void PlayTurn()
        {
            m_InputController.SubscribeToInput(InputCallBack);
            m_InputController.gameObject.SetActive(true);
        }

        private void InputCallBack(int row)
        {
            m_InputController.gameObject.SetActive(false);
            m_InputController.UnSubscribeToInput();
            mPerformAction?.Invoke(row,false);
        }
    }
}
