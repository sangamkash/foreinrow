﻿namespace FourInRow
{
    /// <summary>
    /// This is a temp solution
    /// </summary>
    public static class GameConstants
    {
        //SceneName
        public const string SCENE_STARTUP = "StartUp";
        public const string SCENE_GAMEPLAY = "GamePlay";

        //PlayerPrefs
        public const string PP_PLAYER_NAME = "PlayerName";
        public const string PP_GRID_SIZE = "GridSize";
        public const string PP_MIN_TO_WIN = "MinToWin";
        public const string PP_DIFFICULTY_LEVEL = "DifficultyLevel";
        //Other
        public const string AI_BOT_NAME = "AI Bot";
        public const string DEFAULT_NAME = "Jim";
    }
}

