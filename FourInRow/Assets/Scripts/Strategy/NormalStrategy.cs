﻿using System.Collections;
using System.Collections.Generic;
using FourInRow.Manager;
using FourInRow.Matrix;
using UnityEngine;

namespace FourInRow.AIStrategy
{
    public class NormalStrategy : IStrategy
    {
        //Depedency
        private int[,] pMatrix => GameManager.gameMatrix;
        private int mSize;
        private int mMinToWin;
        public NormalStrategy(int mSizeOfMatrix,int minToWin)
        {
            mSize = mSizeOfMatrix;
            mMinToWin = minToWin;
        }
        /// <summary>
        /// There is 50 % percent chance to choose wisely
        /// </summary>
        /// <returns></returns>
        public int ReturnBestChoice()
        {
            if (Random.Range(0, 2) > 0)
            {
                var validInput = false;
                var row = 0;
                while (validInput==false)
                {
                    var column = 0;
                    row = Random.Range(0, mSize);
                    validInput = pMatrix.ValidateInput(mSize, row, out column);
                }
                return row;
            }
            var mat = pMatrix.ReturnMatrixCopy(mSize);
            var value = 0;
            var listOfValidInput = new List<int>();
            //check for Win first
            for (int i = 0; i < mSize; i++)
            {
                var coloum = 0;
                if (mat.ValidateInput(mSize,i,out coloum))
                {
                    listOfValidInput.Add(i);
                    mat.UpdateValueInRow(mSize,i,true);
                    if (mat.CheckForWinCase(mSize, mMinToWin))
                    {
                        return i;
                    }
                    mat.UndoValueInRow(mSize,i);
                }
            }
            //then Check For oppoent Win;
            for (int i = 0; i < mSize; i++)
            {
                var coloum = 0;
                if (mat.ValidateInput(mSize,i,out coloum))
                {
                    mat.UpdateValueInRow(mSize,i,false);
                    if (mat.CheckForWinCase(mSize, mMinToWin))
                    {
                        return i;
                    }
                    mat.UndoValueInRow(mSize,i);
                }
            }

            var count = listOfValidInput.Count;
            if (count > 0)
                value = listOfValidInput[Random.Range(0, count)];
            return value;
        }
    }
}
