﻿
namespace FourInRow.AIStrategy
{
    [System.Serializable]
    public enum StrategyType
    {
        Easy,
        Normal,
        Hard
    }
    public interface IStrategy 
    {
        int ReturnBestChoice();
    }
}
