﻿using FourInRow.Manager;
using UnityEngine;
using FourInRow.Matrix;
namespace FourInRow.AIStrategy
{
    public class EasyStrategy : IStrategy
    {
        //Depedency
        private int[,] pMatrix => GameManager.gameMatrix;
        private int mSize;
        private int mMinToWin;

        public EasyStrategy(int mSizeOfMatrix,int minToWin)
        {
            mSize = mSizeOfMatrix;
            mMinToWin = minToWin;
        }
        /// <summary>
        /// This is purely random 
        /// </summary>
        /// <returns></returns>
        public int ReturnBestChoice()
        {
            var validInput = false;
            var row = 0;
            while (validInput==false)
            {
                var column = 0;
                 row = Random.Range(0, mSize);
                validInput = pMatrix.ValidateInput(mSize, row, out column);
            }
            return row;
        }
    }
}
