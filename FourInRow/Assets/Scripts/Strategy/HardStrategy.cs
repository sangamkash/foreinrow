﻿using System.Collections.Generic;
using FourInRow.Manager;
using FourInRow.Matrix;
using UnityEngine;

namespace FourInRow.AIStrategy
{
    public class HardStrategy : IStrategy
    {
        //Depedency
        private int[,] pMatrix => GameManager.gameMatrix;
        private int mSize;
        private int mMinToWin;

        public HardStrategy(int mSizeOfMatrix,int minToWin)
        {
            mSize = mSizeOfMatrix;
            mMinToWin = minToWin;
        }
        public int ReturnBestChoice()
        {
            var mat = pMatrix.ReturnMatrixCopy(mSize);
            var value = 0;
            var listOfValidInput = new List<int>();
            //check for Win first
            for (int i = 0; i < mSize; i++)
            {
                var coloum = 0;
                if (mat.ValidateInput(mSize,i,out coloum))
                {
                    listOfValidInput.Add(i);
                    mat.UpdateValueInRow(mSize,i,true);
                    if (mat.CheckForWinCase(mSize, mMinToWin))
                    {
                        return i;
                    }
                    mat.UndoValueInRow(mSize,i);
                }
            }
            //then Check For oppoent Win;
            for (int i = 0; i < mSize; i++)
            {
                var coloum = 0;
                if (mat.ValidateInput(mSize,i,out coloum))
                {
                    mat.UpdateValueInRow(mSize,i,false);
                    if (mat.CheckForWinCase(mSize, mMinToWin))
                    {
                        return i;
                    }
                    mat.UndoValueInRow(mSize,i);
                }
            }

            var count = listOfValidInput.Count;
            if (count > 0)
                value = listOfValidInput[Random.Range(0, count)];
            return value;
        }
    }
}
