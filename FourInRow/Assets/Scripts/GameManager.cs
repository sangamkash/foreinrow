﻿using FourInRow.AIStrategy;
using FourInRow.Matrix;
using FourInRow.Token;
using UnityEngine;
using UnityEngine.UI;

namespace FourInRow.Manager
{
    public class GameManager : MonoBehaviour
    {
        public static int[,] gameMatrix { get; set; }
        
        [Header("Config")] 
        [SerializeField] private int m_BoardSize;
        [SerializeField] private int m_MinToWin;
        
        [Header("Strategy")] 
        [SerializeField] private StrategyType m_StrategyType;

        [Header("Visual Config")] 
        [SerializeField] private int m_BoardBorder;

        [Header("ExternalDependence")] 
        [SerializeField] private Board m_Board;
        [SerializeField] private InputController mInputController;
        [SerializeField] private TokenSpawner m_TokenSpawner;
        [SerializeField] private ResultScreen m_ResultScreen;
        [SerializeField] private bool m_AnimationInProgress;
        [SerializeField] private Player.Player m_LocalPlayer;
        [SerializeField] private Player.Player m_AIPlayer;
        
        [Header("UI Ref")]
        [SerializeField] private Text m_PlayerName;
        [SerializeField] private GameObject m_ShowTurnGameObj;
        [SerializeField] private Text m_metaData;
        private const int NoOfPlayer = 2;
        private int mSteps;

        private Player.Player GetCurrentPlayer
        {
            get
            {
                var value = mSteps % NoOfPlayer;
                m_ShowTurnGameObj.SetActive(value==1);
                return value == 0 ? m_AIPlayer : m_LocalPlayer;
            }
        }

        private void Start()
        {
            Init();
            mSteps = -1;
            MoveToNextPlayer();
        }

        private void MoveToNextPlayer()
        {
            mSteps++;
            GetCurrentPlayer.PlayTurn();
        }

        private void RetryInValid()
        {
            GetCurrentPlayer.PlayTurn();
        }

        private void Init()
        {
            if (PlayerPrefs.HasKey(GameConstants.PP_GRID_SIZE))
                m_BoardSize = PlayerPrefs.GetInt(GameConstants.PP_GRID_SIZE);
            if (PlayerPrefs.HasKey(GameConstants.PP_MIN_TO_WIN))
                m_MinToWin = PlayerPrefs.GetInt(GameConstants.PP_MIN_TO_WIN);
            if (PlayerPrefs.HasKey(GameConstants.PP_MIN_TO_WIN))
            {
                var strategyType = PlayerPrefs.GetInt(GameConstants.PP_DIFFICULTY_LEVEL);
                m_StrategyType = (StrategyType) strategyType;
            }

            var playername = GameConstants.DEFAULT_NAME;
            if (PlayerPrefs.HasKey(GameConstants.PP_PLAYER_NAME))
            {
                playername = PlayerPrefs.GetString(GameConstants.PP_PLAYER_NAME);
            }

            m_metaData.text = $"Meta data --\n Grid size {m_BoardSize} \n min to Win {m_MinToWin}";
            m_PlayerName.text = playername;
            gameMatrix = new int[m_BoardBorder, m_BoardBorder];
           
            gameMatrix.CreateWithDefaultMatrix(m_BoardSize);
            m_Board.DisplayGrid(m_BoardSize, m_BoardBorder);
            mInputController.Init(m_BoardSize, m_BoardBorder);
            m_TokenSpawner.Init(m_BoardSize, m_BoardBorder);
            m_LocalPlayer.Init(PerformAction, null,playername);
            m_AIPlayer.Init(PerformAction,RetrunStrategyByType(m_StrategyType),GameConstants.AI_BOT_NAME);
           
        }
        private void PerformAction(int row, bool isAI)
        {
#if DEBUG_LOG
            Debug.Log($"InputCallBack {row}");
#endif
            int column;
            var isValidate = gameMatrix.ValidateInput(m_BoardSize, row, out column);
            if (m_AnimationInProgress == false && isValidate)
            {
                m_AnimationInProgress = true;

                gameMatrix.UpdateValueInRow(m_BoardSize, row, isAI);
                m_TokenSpawner.SpawnToker(row, column, isAI, () =>
                {
                    m_AnimationInProgress = false;
                    if (mSteps + 1 >= m_BoardSize * m_BoardSize)
                        ShowTie();
                    else if (gameMatrix.CheckForWinCase(m_BoardSize, m_MinToWin) == false)
                        MoveToNextPlayer();
                    else
                        DisplayWin();
                });
            }
            else if (!isValidate)
            {
                RetryInValid();
            }
        }

        private void DisplayWin()
        {
            m_ResultScreen.ShowResult(false, GetCurrentPlayer._PlayerName);
        }

        private void ShowTie()
        {
            m_ResultScreen.ShowResult(true, GetCurrentPlayer._PlayerName);
        }

        private IStrategy RetrunStrategyByType(StrategyType strategyType)
        {
            switch (strategyType)
            {
                case  StrategyType.Easy:
                    return new EasyStrategy(m_BoardSize,m_MinToWin);
                case  StrategyType.Normal:
                    return new NormalStrategy(m_BoardSize,m_MinToWin);
                case  StrategyType.Hard:
                    return new HardStrategy(m_BoardSize,m_MinToWin);
            }

            return null;
        }

#if UNITY_EDITOR
        public int GetBoardSize()
        {
            return m_BoardSize;
        }
#endif
    }
}