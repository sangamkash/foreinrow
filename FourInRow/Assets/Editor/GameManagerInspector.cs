﻿using System.Collections;
using System.Collections.Generic;
using FourInRow.Manager;
using UnityEditor;
using UnityEngine;

namespace FourInRow.Editor
{
    [CustomEditor(typeof(GameManager))]
    [CanEditMultipleObjects]
    public class GameManagerInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var gm = (GameManager) target;
            var count = gm.GetBoardSize();
            if (GameManager.gameMatrix != null)
            {
                DrawMatrix(GameManager.gameMatrix,count,count);
            }
            base.OnInspectorGUI();
        }
        
        private void  DrawMatrix(int[,] matrix,int row,int column)
        {
            GUILayout.Label("Matrix");
            for (int i = 0; i < row; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    for (int j = 0; j < column; j++)
                    {
                        var value = matrix[i, j];
                        var str = "-";
                        switch (value)
                        {
                            case 1:
                                str = "X";
                                break;
                            case -1:
                                str = "0";
                                break;
                        }
                        GUILayout.Label($"({i},{j})={str}");
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            GUILayout.Space(10);
        }
    }
    }
    
   