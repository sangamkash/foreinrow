﻿using System.Collections;
using System.Collections.Generic;
using FourInRow;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace FourInRow.Editor
{
    [CustomEditor(typeof(Board))]
    [CanEditMultipleObjects]
    public class BoardInspector : UnityEditor.Editor
    {
        private int boardSize = 3;
        private int boardPadding = 20;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            boardSize = EditorGUILayout.IntField("BoardSize", boardSize);
            boardPadding = EditorGUILayout.IntField("boardPadding", boardPadding);
            var board=(Board) target;
            if (GUILayout.Button("SetGrid"))
            {
                board.Reset();
                board.DisplayGrid(boardSize,boardPadding);
            }
        }

        private void InputReceived(int value)
        {
#if DEBUG_LOG
            Debug.Log($"InputCallBack {value}");
#endif
        }
    }
}