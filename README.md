Naming convention used for variable  

Public variable Start with prefix _
    Example public int _Data;
Private variable start with prefix m
    Example private int mData;
[SerializeField] private variable start with prefix m_
    Example [SerializeField] private m_Data;
Public property variable start with p_
    Example public int p_Data{get;set;}
private property variable start with p
    Example private int pData{get;set;}
[SerializeField] private property variable start with p
    Example private int pData{get;set;}


**General Description** : 
This game is very similar to four in row ,but the different is you can configure the matrix and also define the min value to win in a matrix
